<?php
	// echo file_get_contents("smartbin.html");
	$url = file_get_contents('http://localhost/services/pipe/read.php');
	$obj = json_decode($url, true);
	$row = $obj['records'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Waste and Water Management Admin</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<!-- The sidebar -->
	<div class="sidebar">
	  <a href="../smartbin">Smartbin</a>
	  <a class="active" href="#Pipe">Pipe</a>
	  <a href="../quality">Quality</a>
	</div>

	<!-- Page content -->
	<div class="content">
	  	<!-- The card choices -->
		<div style="padding-top: 50px">
			<h2 class="text-center">Smart Water Distribution's Admin</h2>
		</div>
		<div style="overflow-x: auto;" class="all-items-center">
			<table style="width:100%">
			  <tr>
			    <th>Pipe's Id</th>
			    <th>Location</th> 
			    <th>Voltage</th>
			  </tr>
				<?php foreach ($row as $a) {?>
			  <tr>
				    <td><?php echo $a['id_pipe']; ?></td>
				  <td><?php echo $a['location']; ?></td> 
				  <td><?php echo $a['voltage']; ?></td>
				</tr>
				<?php } ?>
			  <!-- <tr>
			    <td>Labtek 5</td>
			    <td>tbd</td> 
			    <td>tbd</td>
			    <td class="text-center">0</td>
			  </tr> -->
			</table>
		</div>
	</div>
</body>
</html>