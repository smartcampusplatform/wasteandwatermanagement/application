<?php
	// echo file_get_contents("smartbin.html");
	$url = file_get_contents('http://localhost/services/smartbin/read.php');
	$obj = json_decode($url, true);
	$row = $obj['records'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Waste and Water Management Admin</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<!-- The sidebar -->
	<div class="sidebar">
	  <a class="active" href="#Smartbin">Smartbin</a>
	  <a href="../pipe">Pipe</a>
	  <a href="../quality">Quality</a>
	</div>

	<!-- Page content -->
	<div class="content">
	  	<!-- The card choices -->
		<div style="padding-top: 50px">
			<h2 class="text-center">Smartbin's Admin</h2>
		</div>
		<div style="overflow-x: auto;" class="all-items-center">
			<table style="width:100%">
			  <tr>
			    <th>Smartbin's Location</th>
			    <th>Latitude</th> 
			    <th>Longitude</th>
			    <th>Status</th>
			  </tr>
				<?php foreach ($row as $a) {?>
			  <tr>
					<td><?php echo $a['id_bin']; ?></td>
				  <td><?php echo $a['lat']; ?></td> 
				  <td><?php echo $a['lng']; ?></td>
				  <td class='text-center'>
						<?php echo $a['status']; ?>
						<button class="button0">0</button>
						<button class="button1">1</button>
						<button class="button2">2</button>
					</td>
				</tr>
				<?php } ?>
			  <!-- <tr>
			    <td>Labtek 5</td>
			    <td>tbd</td> 
			    <td>tbd</td>
			    <td class="text-center">0</td>
			  </tr> -->
			</table>
		</div>
	</div>
</body>
</html>